There are a few steps to start the project:
    1) clone project from git
    2) in the project root execute: docker-compose up -d
    3) execute: docker-compose exec php bash
    4) inside container execute: composer install
    5) create database "messages"
    6) import sql dump from path: path-to-clone-project/database/dump_messages.sql
    7) add 127.0.0.1 messages.local to your local hosts
    8) open messages.local/ in your browser