<?php

use app\Service\Request\Request;
use app\Service\Enviroment\EnvLoader;
use app\Service\FileReader\Yaml\YamlReader;
use app\Service\Request\RequestHandler;
use app\Service\Routing\Router;

spl_autoload_register(function ($className) {
    $className = str_replace('\\', '/', $className).'.php';
    require_once $className;
});
require 'vendor/autoload.php';

EnvLoader::load();

$requestHandler = new RequestHandler(Request::getInstance(), YamlReader::getInstance(), Router::getInstance());

$response = $requestHandler->handle();
$response->send();