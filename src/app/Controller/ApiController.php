<?php

namespace app\Controller;

/**
 * Class ApiController
 * @package app\Controller
 */
class ApiController extends AbstractMessageController
{
    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return 'content.php';
    }
}