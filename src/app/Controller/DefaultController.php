<?php

namespace app\Controller;

use app\Service\Pagination\Pagination;
use app\Service\Response\HTMLResponse;
use app\Util\Manager\MessageManager;


/**
 * Class DefaultController
 * @package app\Controller
 */
class DefaultController extends AbstractMessageController
{
    /**
     * @return HTMLResponse
     */
    public function index()
    {
        $messageManager = new MessageManager();
        $messages = $messageManager->findAll();
        $paginator = new Pagination();
        list($pagination, $messages) = $paginator->paginate($messages);

        $view = $this->render('index.php', [
            'messages' => $messages,
            'pagination' => $pagination
        ]);

        return new HTMLResponse($view);
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return 'index.php';
    }
}