<?php

namespace app\Controller;

use app\Service\View\ViewService;

class Controller
{
    /** @var ViewService  */
    protected $view;

    /**
     * Controller constructor.
     * @param $view
     */
    public function __construct(ViewService $view)
    {
        $this->view = $view;
    }

    /**
     * @param string $template
     * @param array $args
     * @return false|string
     */
    protected function render(string $template, array $args = [])
    {
        return $this->view->render($template, $args);
    }
}