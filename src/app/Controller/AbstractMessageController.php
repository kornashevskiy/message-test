<?php

namespace app\Controller;

use app\Service\Pagination\Pagination;
use app\Service\Request\Request;
use app\Service\Response\HTMLResponse;
use app\Service\Validator\MessageValidator;
use app\Service\Validator\UserValidator;
use app\Service\Validator\ValidatorService;
use app\Util\Manager\MessageManager;
use app\Util\Manager\UserManager;

abstract class AbstractMessageController extends Controller
{
    /**
     * @param Request $request
     * @return HTMLResponse
     * @throws \app\Service\Exception\ValidationException
     */
    public function addMessage(Request $request)
    {
        $userManager = new UserManager();
        $messageManager = new MessageManager();

        $validator = new ValidatorService();
        $validator
            ->add(new UserValidator($request))
            ->add(new MessageValidator($request));
        $validator->validate();

        $fullName = strip_tags($request->post('fullname'));
        $arrayFullName = explode(' ', $fullName);

        // validation that full name has space
        $errors = [];
        if (count($arrayFullName) !== 2) {
            $errors[] = ['fullname' => 'Please fill correct full name'];
        }

        // validation birthday date format
        try {
            new \DateTime(strip_tags($request->post('birthday')));
        } catch (\Exception $e) {
            $errors[] = ['birthday' => 'wrong format'];
        }

        if ($validator->fails()) {
            $errors = array_merge($errors, $validator->getErrors());
        }

        if (empty($errors)) {
            $birthday = strip_tags($request->post('birthday'));
            $email = strip_tags($request->post('email'));
            $message = strip_tags($request->post('message'));

            if ($user = $userManager->findOneBy(['email' => $email])) {
                $messageManager->addMessage($user->id, $message);
            } else {
                $userId = $userManager->addUser(
                    $arrayFullName[0],
                    $arrayFullName[1],
                    $birthday,
                    $email
                );
                $messageManager->addMessage($userId, $message);
            }
        }

        $messages = $messageManager->findAll();
        $paginator = new Pagination();
        list($pagination, $messages) = $paginator->paginate($messages);

        $view = $this->render($this->getTemplate(), [
            'fields' => [
                'fullname' => $request->post('fullname'),
                'email' => $request->post('email'),
                'birthday' => $request->post('birthday'),
                'message' => $request->post('message')
            ],
            'errors' => $errors,
            'messages' => $messages,
            'pagination' => $pagination,
            'success' => empty($errors) ? true : false
        ]);

        return new HTMLResponse($view);
    }

    /** @return string */
    abstract public function getTemplate(): string;
}