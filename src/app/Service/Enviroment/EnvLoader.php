<?php

namespace app\Service\Enviroment;

/**
 * Class EnvLoader
 * @package app\Service\Enviroment
 */
class EnvLoader
{
    const ENV_PATH = __DIR__.'/../../../.env';

    /**
     * Put environment vars to php
     */
    public static function load()
    {
        $content = explode("\r\n", file_get_contents(self::ENV_PATH));

        foreach ($content as $item) {
            putenv($item);
        }
    }
}