<?php

namespace app\Service\Validator;

use app\Service\Request\Request;
use Rakit\Validation\Validation;
use \Rakit\Validation\Validator as ValidatorService;

abstract class AbstractValidator implements Validator
{
    /** @var ValidatorService  */
    protected $validator;
    /** @var Request  */
    protected $request;

    /**
     * AbstractValidator constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->validator = new ValidatorService();
        $this->request = $request;
    }

    /**
     * @return Validation
     */
    public function validate()
    {
        /** @var Validation $validation */
        $validation = $this->makeRules();
        $validation->validate();

        return $validation;
    }

    /**
     * @return Validation
     */
    abstract protected function makeRules(): Validation;
}