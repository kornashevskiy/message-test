<?php

namespace app\Service\Validator;

use Rakit\Validation\Validation;

/**
 * Class MessageValidator
 * @package app\Service\Validator
 */
class MessageValidator extends AbstractValidator
{
    /**
     * @return Validation
     */
    protected function makeRules(): Validation
    {
        return $this->validator->make($this->request->getPost(), [
            'message' => 'required'
        ]);
    }
}