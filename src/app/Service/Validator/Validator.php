<?php

namespace app\Service\Validator;

interface Validator
{
    public function validate();
}