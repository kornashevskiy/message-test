<?php

namespace app\Service\Validator;

use Rakit\Validation\Validation;

/**
 * Class UserValidator
 * @package app\Service\Validator
 */
class UserValidator extends AbstractValidator
{
    /**
     * @return Validation
     */
    protected function makeRules(): Validation
    {
        return $this->validator->make($this->request->getPost(), [
            'fullname' => 'required|max:40',
            'email' => 'email',
            'birthday' => 'required'
        ]);
    }
}