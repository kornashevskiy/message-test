<?php

namespace app\Service\Validator;

use app\Service\Exception\ValidationException;
use Rakit\Validation\ErrorBag;
use Rakit\Validation\Validation;

/**
 * Class ValidatorService
 * @package app\Service\Validator
 */
class ValidatorService
{
    /** @var  */
    protected $validators;
    /** @var array */
    protected $errors = [];

    /**
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];

        foreach ($this->errors as $key => $error) {
            foreach ($error as $field => $array) {
                $errors[] = [$field => $array[key($array)]];
            }
        }
        return $errors;
    }

    /**
     * @param Validator $validator
     * @return $this
     */
    public function add(Validator $validator)
    {
        $this->validators[] = $validator;

        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function validate()
    {
        if (is_array($this->validators)) {
            /** @var Validator $validator */
            foreach ($this->validators as $validator) {
                /** @var Validation $validation */
                $validation = $validator->validate();

                if ($validation->fails()) {
                    $this->errors[] = $validation->errors()->toArray();
                }
            }
        } else {
            throw new ValidationException('there are no validators in list');
        }

        return;
    }

    /**
     * @return bool
     */
    public function fails()
    {
        return !empty($this->errors) ? true : false;
    }
}