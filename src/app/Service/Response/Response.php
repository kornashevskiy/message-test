<?php

namespace app\Service\Response;

abstract class Response
{
    protected $body;
    protected $code;
    protected $headers;

    /**
     * Response constructor.
     * @param $body
     */
    public function __construct(string $body, int $code = 200, array $headers = [])
    {
        $this->body = $body;
        $this->code = $code;
        $this->headers = $headers;
    }

    /**
     * @return Response
     */
    public function send(): Response
    {
        $this
            ->setHeaders()
            ->sendContent();

        return $this;
    }

    /**
     * @return Response
     */
    public function sendContent(): Response
    {
        echo $this->body;

        return $this;
    }

    /** return Response */
    abstract function setHeaders(): Response;
}