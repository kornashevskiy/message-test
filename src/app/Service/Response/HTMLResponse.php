<?php

namespace app\Service\Response;

/**
 * Class HTMLResponse
 * @package app\Service\Response
 */
class HTMLResponse extends Response
{
    const HEADERS = [
        'Content-Type' => 'text/html; charset=utf-8'
    ];

    /**
     * HTMLResponse constructor.
     * @param string $body
     * @param int $code
     * @param array $headers
     */
    public function __construct(string $body, int $code = 200, array $headers = [])
    {
        parent::__construct($body, $code, $headers);
        $this->headers = array_merge(self::HEADERS, $this->headers);
    }

    /**
     * @return Response
     */
    function setHeaders(): Response
    {
        foreach ($this->headers as $key => $value) {
            header(sprintf('%s:%s', $key, $value));
        }

        return $this;
    }
}