<?php

namespace app\Service\View;

class ViewService
{
    const STORAGE = __DIR__.'/../../Web/template/';

    /**
     * @param string $template
     * @param array $vars
     * @return false|string
     */
    public function render(string $template, array $vars = [])
    {
        $template = self::STORAGE.$template;
        ob_start();
        extract($vars);
        require($template);

        return ob_get_clean();
    }
}