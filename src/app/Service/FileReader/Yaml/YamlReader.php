<?php

namespace app\Service\FileReader\Yaml;

use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlReader
 * @package app\Service\FileReader\Yaml
 */
class YamlReader
{
    /** @var string  */
    protected $filePath;
    /** @var array */
    protected $content;
    /** @var YamlReader */
    protected static $instance;

    /**
     * YamlReader constructor.
     */
    private function __construct()
    {
        $this->filePath = sprintf('%s/../../../%s', __DIR__, getenv('CONFIG_FILE_PATH'));
        $this->content = Yaml::parseFile($this->filePath);
    }

    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @return array|null
     */
    public function getRouting(): ?array
    {
        if (isset($this->content['routing'])) {
            return $this->content['routing'];
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getDatabase(): ?array
    {
        if (isset($this->content['database'])) {
            return $this->content['database'];
        }

        return null;
    }
}