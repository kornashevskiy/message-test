<?php

namespace app\Service;

use app\Service\FileReader\Yaml\YamlReader;

/**
 * Class DBConnection
 * @package app\Service
 */
class DBConnection
{
    /** @var \PDO */
    protected static $pdo;

    private function __construct()
    {}

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (!isset(static::$pdo)) {
            $connectionParams = YamlReader::getInstance()
                ->getDatabase();
            try {
                $dsn = sprintf('mysql:host=%s;dbname=%s', $connectionParams['host'], $connectionParams['dbname']);

                /** @var \PDO pdo */
                static::$pdo = new \PDO($dsn, $connectionParams['username'], $connectionParams['password']);
                static::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $e) {
                echo "Connection failed: ".$e->getMessage();
            }
        }

        return static::$pdo;
    }
}