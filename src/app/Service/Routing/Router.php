<?php

namespace app\Service\Routing;

use app\Service\Exception\RoutingConfigsException;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

/**
 * Class Router
 * @package app\Service\Routing
 */
class Router
{
    /** @var array  */
    protected $routesData;

    /** @var Router */
    protected static $instance;

    /** @var Dispatcher */
    protected $dispatcher;

    /**  */
    private function __construct(){}

    /**
     * @return Router
     */
    public static function getInstance(): Router
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @return mixed
     */
    public function getDispatcher(): Dispatcher
    {
        return $this->dispatcher;
    }

    /**
     * @param array $routesData
     * @return Router
     */
    public function setRoutesData(array $routesData): Router
    {
        $this->routesData = $routesData;

        return $this;
    }

    /**
     * @return Router
     * @throws RoutingConfigsException
     */
    public function init(): Router
    {
        $data = $this->routesData;

        if (empty($data)) {
            throw new RoutingConfigsException('routing parameters where not defined');
        }

        $this->dispatcher = simpleDispatcher(function (RouteCollector $router) use ($data) {
            foreach ($data as $controllerClass => $actions) {
                foreach ($actions as $params) {
                    $handler = sprintf('%s@%s', $controllerClass, $params['action']);
                    $router->addRoute($params['method'], $params['path'], $handler);
                }
            }
        });

        return $this;
    }

    /**
     * @param string $string
     * @return array
     */
    public function controllerInfo(string $string)
    {
        $result = [];

        $controllerData = explode('@', $string);
        if (count($controllerData) == 2) {
            $result = [
                'controller' => $controllerData[0],
                'action' => $controllerData[1],
            ];
        }

        return $result;
    }
}