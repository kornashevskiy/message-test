<?php

namespace app\Service\Pagination;

/**
 * Class Pagination
 * @package app\Service\Pagination
 */
class Pagination
{
    /** @var \Zebra_Pagination  */
    protected $paginator;

    const RECORDS_PER_PAGE = 5;

    /**
     * Pagination constructor.
     */
    public function __construct()
    {
        $this->paginator = new \Zebra_Pagination();
    }

    public function paginate(array $items)
    {
        $this->paginator->base_url('/');
        $this->paginator->records(count($items));
        $this->paginator->records_per_page(self::RECORDS_PER_PAGE);

        $items = array_slice(
            $items,
            (($this->paginator->get_page() - 1) * self::RECORDS_PER_PAGE),
            self::RECORDS_PER_PAGE
        );

        return [$this->paginator, $items];
    }
}