<?php

namespace app\Service\Request;

/**
 * Class Request
 * @package app\Service
 */
class Request
{
    /** @var Request */
    private static $instance;
    /** @var array */
    protected $post;
    /** @var array */
    protected $get;
    /** @var array */
    protected $server;
    /** @var array */
    protected $file;
    /** @var array */
    protected $cookie;
    /** @var array */
    protected $session;

    /**
     * Request constructor.
     */
    private function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->server = $_SERVER;
        $this->file = $_FILES;
        $this->cookie = $_COOKIE;
        $this->session = $_SESSION;
    }

    /**
     * @return Request
     */
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @return array
     */
    public function getPost(): array
    {
        return $this->post;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function post(string $key)
    {
        return isset($this->post[$key]) ? $this->post[$key] : null;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        return isset($this->get[$key]) ? $this->get[$key] : null;
    }

    /**
     * @return mixed
     */
    public function getRequestUri()
    {
        return $this->server['REQUEST_URI'];
    }

    /**
     * @return mixed
     */
    public function method()
    {
        return $this->server['REQUEST_METHOD'];
    }
}