<?php

namespace app\Service\Request;

use app\Controller\Controller;
use app\Service\Exception\NotFoundException;
use app\Service\Exception\RoutingConfigsException;
use app\Service\FileReader\Yaml\YamlReader;
use app\Service\Response\Response;
use app\Service\Routing\Router;
use app\Service\View\ViewService;
use FastRoute\Dispatcher;
use http\Exception\BadMethodCallException;

/**
 * Class RequestHandler
 * @package app\Service\Request
 */
class RequestHandler
{
    /** @var Request  */
    protected $request;
    /** @var YamlReader  */
    protected $yamlReader;
    /** @var Router  */
    protected $router;

    /**
     * RequestHandler constructor.
     * @param Request $request
     * @param YamlReader $yamlReader
     * @param Router $router
     */
    public function __construct(Request $request, YamlReader $yamlReader, Router $router)
    {
        $this->request = $request;
        $this->yamlReader = $yamlReader;
        $this->router = $router;
    }

    /**
     * @return Response
     * @throws NotFoundException
     * @throws RoutingConfigsException
     */
    public function handle(): Response
    {
        $routingData = $this->yamlReader->getRouting();
        $this->router->setRoutesData($routingData)
            ->init();

        $routeInfo = $this->router->getDispatcher()
            ->dispatch($this->request->method(), $this->request->getRequestUri());

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                throw new NotFoundException('this page can not be fount', 404);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                throw new BadMethodCallException('Method not allowed');
                break;
            case Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                array_unshift($vars, $this->request);

                $controllerData = $this->router->controllerInfo($handler);
                if (empty($controllerData)) {
                    throw new RoutingConfigsException(sprintf('bad controller configuration: "%s"', $handler));
                }
                /** @var Controller $controller */
                $controller = new $controllerData['controller'](new ViewService());
                /** @var Response $response */
                $response = call_user_func_array([$controller, $controllerData['action']], $vars);
                break;
            default:
                throw new RoutingConfigsException(sprintf('router return undefined response: "%s"', $routeInfo[0]));
        }

        return $response;
    }
}