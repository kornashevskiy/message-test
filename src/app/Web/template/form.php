<?php
    if (isset($errors) && !empty($errors)) {
        echo "<div id=\"errors\"><ul>";
        foreach ($errors as $items) {
            foreach ($items as $field => $error) {
                echo "<li><b class='fieldName'>{$field}</b> - {$error}</li>";
            }
        }
        echo "</ul></div>";
    }
?>

<h1>Your post</h1>
<form method="post" action="addMessage" id="message-form">
    <p class="">
        <label for="fullname">Name surname *</label><br/>
        <input id="fullname" type="text" name="fullname" value="<?php if (!$success) echo $fields['fullname'] ?>" required/>
    </p>
    <p>
        <label for="birthday">Date of birth *</label><br/>
        <input id="birthday" type="text" name="birthday" value="<?php if (!$success) echo $fields['birthday'] ?>" required/>
    </p>
    <p>
        <label for="email">E. mailing address</label><br/>
        <input id="email" type="email" name="email" value="<?php if (!$success) echo $fields['email'] ?>" />
    </p>
    <p class="">
        <label for="message">Your message *</label><br/>
        <textarea id="message" name="message" required></textarea>
    </p>
    <p>
        <span>* - required fields</span>
        <input type="submit" value="Submit" />
        <img id="loader" src="app/Web/public/images/ajax-loader.gif" class="d-none"/>
    </p>
</form>

