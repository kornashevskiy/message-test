<div id="messages">
    <ul>
        <?php
        if (empty($messages)) {
            echo "<li><b>There are currently no posts. Be the first!</b></li>";
        } else {
            foreach ($messages as $message) {
                $now = new DateTime();
                $birthday = new DateTime($message->birthday);
                $interval = $birthday->diff($now);

                if (isset($message->email)) {
                    $fillName = "<a href=\"mailto:{$message->email}\">{$message->first_name} {$message->last_name}</a>";
                } else {
                    $fillName = "{$messages->first_name} {$message->last_name}";
                }
                echo "<li>
                                <strong>{$message->created_at}</strong>
                                $fillName
                                {$interval->y} years old
                                <br>
                                {$message->message}
                              </li>";
            }
        }
        ?>
    </ul>
    <div id="paginator">
        <?php $pagination->render() ?>
    </div>
</div>
