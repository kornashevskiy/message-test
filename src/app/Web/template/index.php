<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Message</title>
    <link rel="stylesheet" media="screen" type="text/css" href="app/Web/css/screen.css" />
    <link rel="stylesheet" media="screen" type="text/css" href="app/Web/css/zebra_pagination.css" />
</head>
<body>
<div id="wrapper">
    <?php include_once('content.php') ?>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="app/Web/js/app.js"></script>
</html>

