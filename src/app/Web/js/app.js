$(document).ready(function () {
    initColorForms();
    initSubmitForm();
});

function initSubmitForm() {
    $('#message-form').submit(function (e) {
        e.preventDefault();
        $('#loader').removeClass('d-none');

        $(this).find('input').each(function () {
            $(this).attr('disabled', 'true');
        });
        $(this).find('textarea').each(function () {
            $(this).attr('disabled', 'true');
        });

        var fullname, birthday, email, message;
        fullname = $('input[name="fullname"]').val();
        birthday = $('input[name="birthday"]').val();
        email = $('input[name="email"]').val();
        message = $('textarea[name="message"]').val();
        //TO DO data = $(this).serialize() - it doesnt works
        $.ajax({
            url: 'api/addMessage',
            data: {
                fullname: fullname,
                birthday: birthday,
                email: email,
                message: message
            },
            method: $(this).attr("method")
        }).done(function (response) {
            $('#wrapper').html(response);
            $(this).find('input').each(function () {
                $(this).removeAttr('disabled');
            });
            $(this).find('textarea').each(function () {
                $(this).removeAttr('disabled');
            });
            $('#loader').addClass('d-none');


            $("#errors li>b.fieldName").each(function() {
                var name = $(this).text();
                $(`input[name="${name}"]`).css({border: '1px solid red'});
            });

            initColorForms();
            initSubmitForm();
        })
    })
}

function initColorForms() {
    $('input').click(function () {
        $(this).css({border: '1px solid blue'})
    });
    $('input').blur(function () {
        $(this).css({border: '1px solid gray'})
    });
    $('textarea').click(function () {
        $(this).css({border: '1px solid blue'})
    });
    $('textarea').blur(function () {
        $(this).css({border: '1px solid gray'})
    });
}