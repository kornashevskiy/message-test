<?php

namespace app\Util\Manager;

use app\Util\Traits\DBConnection;

/**
 * Class UserManager
 * @package app\Util\Manager
 */
class UserManager
{
    use DBConnection;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $birthday
     * @param string|null $email
     * @return string
     * @throws \Exception
     */
    public function addUser(
        string $firstName,
        string $lastName,
        string $birthday,
        string $email = null
    ) {
        $date = new \DateTime($birthday);
        $pdo = $this->getConnection();
        $query = 'insert into users (first_name, last_name, email, birthday) values (?,?,?,?)';
        $stmt = $pdo->prepare($query);
        $stmt->execute([
            $firstName,
            $lastName,
            $email,
            $date->format('Y-m-d')
        ]);

        return $pdo->lastInsertId();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function findOneBy(array $params)
    {
        $pdo = $this->getConnection();
        $query = sprintf("select * from users where %s=:email", key($params));
        $stmt = $pdo->prepare($query);
        $stmt->execute(['email' => $params[key($params)]]);

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        return $result;
    }
}