<?php

namespace app\Util\Manager;

use app\Util\Traits\DBConnection;

/**
 * Class MessageManager
 * @package app\Util\Manager
 */
class MessageManager
{
    use DBConnection;

    /**
     * @return mixed
     */
    public function findAll()
    {
        $pdo = $this->getConnection();

        $query = 'select m.*, u.* from messages m join users u on m.user_id = u.id order by m.id desc';
        $stmt = $pdo->query($query);

        $messages = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return $messages;
    }

    /**
     * @param int $userId
     * @param string $message
     * @throws \Exception
     */
    public function addMessage(int $userId, string $message)
    {
        $pdo = $this->getConnection();
        $query = 'insert into messages (user_id, message, created_at) values(?,?,?)';
        $stmt = $pdo->prepare($query);
        $stmt->execute([$userId, $message, (new \DateTime())->format('Y-m-d')]);
    }
}