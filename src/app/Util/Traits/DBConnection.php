<?php

namespace app\Util\Traits;

trait DBConnection
{
    /**
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return \app\Service\DBConnection::getInstance();
    }
}